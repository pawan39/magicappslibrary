package com.magicapps.utils;

public class DebugUtil {
    private static Boolean isDebuggable=false;
    private static Boolean isInit = false;


    public static void setDebuggable(Boolean debuggable) {
        isDebuggable = debuggable;
        isInit = true;
    }

    public static Boolean isDebbugable() {
        return isDebuggable;
    }

    public static Boolean isDUNotInit(){
        return !isInit;
    }
}
