package com.magicapps.utils;

import android.util.Log;

public class LogUtil {
    private static final String DEBUG = "DEBUG : ";
    private static final String INFO = "INFO : ";
    private static final String ERROR = "ERROR : ";

    public static void debug(String tag, String message) {
        if (DebugUtil.isDebbugable()) {
            Log.d(tag, DEBUG.concat(message));
        }
        handleUninitializedUtil();
    }

    public static void info(String tag, String message) {
        if (DebugUtil.isDebbugable()) {
            Log.i(tag, INFO.concat(message));
        }
        handleUninitializedUtil();
    }

    public static void error(String tag, String message) {
        if (DebugUtil.isDebbugable()) {
            Log.e(tag, ERROR.concat(message));
        }
        handleUninitializedUtil();
    }

    public static void verbose(String tag, String message){
        if(DebugUtil.isDebbugable()){
            Log.v(tag, message);
        }
        handleUninitializedUtil();
    }

    public static void apocalypse(String taa, String message, Throwable throwable){
        if(DebugUtil.isDebbugable()){
            Log.wtf(taa, message, throwable);
        }
        handleUninitializedUtil();
    }

    public static void handleUninitializedUtil(){
        if(DebugUtil.isDUNotInit()){
            Log.w("⚠⚠WARNING⚠⚠", "Debug Util is not initialized");
        }
    }
}
